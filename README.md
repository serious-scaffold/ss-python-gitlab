# Serious Scaffold Python GitLab Demo

GitLab Demo of Serious Scaffold Python

[![pipeline status](https://gitlab.com/serious-scaffold/ss-python-gitlab/badges/main/pipeline.svg)](https://gitlab.com/serious-scaffold/ss-python-gitlab/-/commits/main)
[![coverage report](https://gitlab.com/serious-scaffold/ss-python-gitlab/badges/main/coverage.svg)](https://gitlab.com/serious-scaffold/ss-python-gitlab/-/commits/main)
[![Latest Release](https://gitlab.com/serious-scaffold/ss-python-gitlab/-/badges/release.svg)](https://gitlab.com/serious-scaffold/ss-python-gitlab/-/releases)
[![PyPI](https://img.shields.io/pypi/v/ss-python-gitlab)](https://pypi.org/project/ss-python-gitlab/)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/ss-python-gitlab)](https://pypi.org/project/ss-python-gitlab/)
[![GitLab](https://img.shields.io/gitlab/license/serious-scaffold/ss-python-gitlab?gitlab_url=https%3A%2F%2Fgitlab.com)](https://gitlab.com/serious-scaffold/ss-python-gitlab/-/blob/main/LICENSE)

[![pdm-managed](https://img.shields.io/badge/pdm-managed-blueviolet)](https://pdm-project.org)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![Checked with mypy](https://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-%23FE5196?logo=conventionalcommits&logoColor=white)](https://conventionalcommits.org)
[![Pydantic v2](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/pydantic/pydantic/5697b1e4c4a9790ece607654e6c02a160620c7e1/docs/badge/v2.json)](https://pydantic.dev)
[![Copier](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/copier-org/copier/master/img/badge/badge-grayscale-inverted-border-orange.json)](https://github.com/copier-org/copier)
[![Serious Scaffold Python](https://img.shields.io/endpoint?url=https://serious-scaffold.github.io/ss-python/_static/badges/logo.json)](https://serious-scaffold.github.io/ss-python)
[![Open in Dev Containers](https://img.shields.io/static/v1?label=Dev%20Containers&message=Open&color=blue&logo=visualstudiocode)](https://vscode.dev/redirect?url=vscode://ms-vscode-remote.remote-containers/cloneInVolume?url=https://gitlab.com/serious-scaffold/ss-python-gitlab)

> [!WARNING]
> _Serious Scaffold Python GitLab Demo_ is in the **Alpha** phase.
> Frequent changes and instability should be anticipated.
> Any feedback, comments, suggestions and contributions are welcome!

This is a GitLab demo for Serious Scaffold Python

## 📜 License

MIT License, for more details, see the [LICENSE](https://gitlab.com/serious-scaffold/ss-python-gitlab/-/blob/main/LICENSE) file.
