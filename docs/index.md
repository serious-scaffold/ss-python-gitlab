# Welcome to Serious Scaffold Python GitLab Demo's documentation

```{toctree}
:hidden:
Overview <self>
management/index
development/index
advanced/index
cli/index
api/index
reports/index
Changelog <https://gitlab.com/serious-scaffold/ss-python-gitlab/-/releases>
```

```{include} ../README.md
:start-line: 1
```

## 🔖 Indices and tables

* {ref}`genindex`
* {ref}`modindex`
* {ref}`search`
